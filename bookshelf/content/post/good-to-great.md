+++
title = "Good to Great Book Review"
draft = true
date = "2016-11-16T10:52:09+07:00"

+++

I read **Good to Great in January 2016**. An awesome read sharing detailed analysis on how good companies became great.
